import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        int fistNum, secondNum;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        fistNum = sc.nextInt();
        System.out.print("Please input second number: ");
        secondNum = sc.nextInt();
        if(fistNum > secondNum) {
            for(int i = fistNum; i >= secondNum; i--){
                System.out.print(i + " ");
            }
        }else{
            for(int i = fistNum; i <= secondNum; i++){
                System.out.print(i + " ");
            }
        }
        sc.close();
    }
}
