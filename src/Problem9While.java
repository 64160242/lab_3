import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n ;
        System.out.print("Please input n: ");
        n = sc.nextInt();
        for(int i = 1 ; i <= n ;i++){
            System.out.println("");
            for(int j = 1 ; j <= n ;j++){
                System.out.print(j);
            }
        }
        sc.close();
    }
}
